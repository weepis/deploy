FROM golang:alpine

MAINTAINER Bastien Dhiver <contact@bastn.fr>

ENV APP_PATH=$GOPATH/src/gitlab.com/weepis/deploy

ADD . $APP_PATH
WORKDIR $APP_PATH

RUN apk update
RUN apk add make git

RUN go install -v

ENTRYPOINT $GOPATH/bin/deploy
