package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"

	"github.com/Sirupsen/logrus"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

const (
	ServiceName = "deploy"
)

var (
	errRegisterToKong         = errors.New("could not register to kong")
	errNoRunningKongContainer = errors.New("no running kong container")
	errKongContainerNoIPAddr  = errors.New("kong container has no IP address")
	errMyIPAddrNotFound       = errors.New("could not get the local IP address")
)

type Service struct {
	dockerClient *client.Client
}

func New() (*Service, error) {
	var s Service
	var err error
	client.NewEnvClient()
	s.dockerClient, err = client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		return nil, err
	}
	return &s, nil
}

type kongRegisterService struct {
	Name string `json:"name"`
	Url  string `json:"url"`
}

type kongAddRoute struct {
	Paths []string `json:"paths"`
}

func getMyIPAddr() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}
	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			return ipnet.IP.String(), nil
		}
	}
	return "", errMyIPAddrNotFound
}

func (s *Service) RegisterToKong() error {
	ctx := context.Background()

	// Get kong container ID
	args := filters.NewArgs(filters.Arg("ancestor", "kong"), filters.Arg("status", "running"))
	containers, err := s.dockerClient.ContainerList(ctx, types.ContainerListOptions{Filters: args, Limit: 1})
	if err != nil {
		return err
	}
	if len(containers) != 1 {
		return errNoRunningKongContainer
	}

	info, err := s.dockerClient.ContainerInspect(ctx, containers[0].ID)
	if err != nil {
		return err
	}
	// get IP
	var kongIP string
	for _, net := range info.NetworkSettings.Networks {
		if net.IPAddress != "" {
			kongIP = net.IPAddress
			break
		}
	}
	if kongIP == "" {
		return errKongContainerNoIPAddr
	}

	// Get my IP
	myIP, err := getMyIPAddr()
	if err != nil {
		logrus.WithError(err).Error("getting local IP failed")
		return err
	}
	fmt.Printf("My local IP is: %s\n", myIP)

	// register deploy service
	kongBaseURL := fmt.Sprintf("http://%s:8001/", kongIP)
	kongServiceURL := fmt.Sprintf("%sservices/", kongBaseURL)
	registerData := kongRegisterService{
		Name: ServiceName,
		Url:  fmt.Sprintf("http://%s:8080", myIP),
	}
	jsonData, _ := json.Marshal(&registerData)
	resp, err := http.Post(kongServiceURL, "application/json", bytes.NewReader(jsonData))
	if err != nil {
		logrus.WithError(err).Error("could not add service to kong")
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.WithError(err).Error("ready request body failed")
		return errors.New("tmp error")
	}

	logrus.Infof("Got Kong response: %v, body: %v", resp, string(body))

	if resp.StatusCode != http.StatusCreated {
		logrus.Errorf("expected %d, got %d", http.StatusCreated, resp.StatusCode)
		return err
	}

	// Add route to service
	kongRoutesURL := fmt.Sprintf("%s%s/routes", kongServiceURL, ServiceName)
	routeData := kongAddRoute{
		Paths: []string{"/deploy"},
	}
	jsonData, _ = json.Marshal(&routeData)
	resp, err = http.Post(kongRoutesURL, "application/json", bytes.NewReader(jsonData))
	if err != nil {
		logrus.WithError(err).Error("could not add service to kong")
		return err
	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.WithError(err).Error("ready request body failed")
		return errors.New("tmp error")
	}

	logrus.Infof("Got Kong response: %v, body: %v", resp, string(body))

	return nil
}

func main() {
	srv, err := New()
	if err != nil {
		logrus.WithError(err).Fatalf("could not init %s service", ServiceName)
	}
	if err := srv.RegisterToKong(); err != nil {
		logrus.WithError(err).Fatal(errRegisterToKong.Error())
	}

	// expose test JSON endpoint
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "Hello World, I'm %s", ServiceName)
	})
	if err := http.ListenAndServe(":8080", mux); err != nil {
		logrus.WithError(err).Fatal("could not start http endpoint")
	}
}
